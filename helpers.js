export function getStatusMessage(statusCode) {
    const statusMessages = {
        200: "OK",
        201: "Created",
        404: "Not Found",
    };

    return statusMessages[statusCode] || "Unknown Status";
}