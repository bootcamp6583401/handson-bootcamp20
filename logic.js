let selectedOption = document.getElementById("method");
let enteredText = document.getElementById("urlApi");
let bodyJson = document.getElementById("bodyJson");

let resultDiv = document.getElementById("result");

const requestForm = document.getElementById("request-form");
const responseRes = document.getElementById("response-result");
const requestBody = document.getElementById("request-body");

async function handleSendRequest(event) {
  console.log(event);
  event.preventDefault();

  //   const url = requestForm.elements.url.value;
  //   const method = requestForm.elements.method.value;

  //   console.log({ url, method, requestBody });

  //   let selectedMethod = selectedOption.value;
  //   let enteredUrl = enteredText.value;
  //   let body = bodyJson.value;

  //   const res = await fetch(url, {
  //     method: method,
  //     body: JSON.stringify(requestBody.value),
  //   });

  //   responseRes.innerText = await res.text();
}

requestForm.addEventListener("submit", handleSendRequest);

selectedOption.addEventListener("change", updateSelectedOption);
enteredText.addEventListener("change", updateEnteredText);
bodyJson.addEventListener("change", updateBody);

function updateBody() {
  bodyJson = document.getElementById("bodyJson");
}

function updateSelectedOption() {
  selectedOption = document.getElementById("method");
}

function updateEnteredText() {
  enteredText = document.getElementById("urlApi");
}
