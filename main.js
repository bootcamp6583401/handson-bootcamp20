import "./style.css";
import { getStatusMessage } from "./helpers";

// params menu
const requestMenus = document.querySelectorAll(".request-menu");
const requestMenuContents = document.querySelectorAll(".request-menu-content");

requestMenus.forEach((btn) => {
  btn.addEventListener("click", () => {
    const val = btn.dataset.value;
    requestMenus.forEach((btn) => {
      if (btn.dataset.value === val && !btn.classList.contains("active")) {
        btn.classList.add("active");
      } else if (
        btn.dataset.value !== val &&
        btn.classList.contains("active")
      ) {
        btn.classList.remove("active");
      }
    });
    requestMenuContents.forEach((content) => {
      if (content.dataset.value === val) {
        content.classList.remove("hidden");
      } else if (content.dataset.value !== val) {
        content.classList.add("hidden");
      }
    });
  });
});

// handle requests
const requestForm = document.getElementById("request-form");
const responseRes = document.getElementById("response-result");
const requestBody = document.getElementById("request-body");
const requestMethodSelect = document.getElementById("request-method");
const saveBtn = document.getElementById("save-btn");

const urlInput = requestForm.querySelector('[name="url"]');
console.log(urlInput);

const responseStatus = document.getElementById("response-status");
const responseTime = document.getElementById("response-time");

async function handleSendRequest(event) {
  console.log(event);
  event.preventDefault();

  const url = requestForm.elements.url.value;
  const method = requestForm.elements.method.value;

  let startTime = performance.now();

  const res = await fetch(url, {
    method: method,
    body:
      method.toLowerCase() === "post" ||
      method.toLowerCase() === "put" ||
      method.toLowerCase() === "patch"
        ? requestBody.value
        : undefined,
    headers: {
      ...(method.toLowerCase() === "post" ||
      method.toLowerCase() === "put" ||
      method.toLowerCase() === "patch"
        ? {
            "Content-Type": "application/json",
          }
        : {}),
    },
  });

  let endTime = performance.now();

  let responseTimeValue = endTime - startTime;

  responseRes.innerText = await res.text();
  responseStatus.innerText = res.status + " " + getStatusMessage(res.status);
  responseTime.innerText = responseTimeValue.toFixed() + " ms ";
}

const paramTableBody = document.querySelector("#param-table-body");

const lastParamRow = document.querySelector(".param-row:last-child");

const paramKey = lastParamRow.querySelector(".param-row__key");

const addCheckboxListener = () => {
  document.querySelectorAll(".param-row__check").forEach((ch) => {
    if (!ch.dataset.outdated) {
      ch.addEventListener("change", (e) => {
        formatQuery();
      });
    }
  });
};

addCheckboxListener();

const handleAddParamRow = () => {
  const newParamRow = document.createElement("tr");
  newParamRow.classList.add("param-row");

  const paramCheckData = document.createElement("td");
  paramCheckData.classList.add("table-cell");
  const paramCheck = document.createElement("input");
  paramCheck.type = "checkbox";
  paramCheck.classList.add("param-row__check");
  paramCheckData.appendChild(paramCheck);
  newParamRow.appendChild(paramCheckData);

  const paramKeyData = document.createElement("td");
  paramKeyData.classList.add("table-cell");
  const paramKey = document.createElement("input");
  paramKey.type = "text";
  paramKey.classList.add("param-row__key");
  paramKey.className = "bg-transparent outline-none param-row__key";
  paramKeyData.appendChild(paramKey);
  newParamRow.appendChild(paramKeyData);

  const paramDescData = document.createElement("td");
  paramDescData.classList.add("table-cell");
  const paramDesc = document.createElement("input");
  paramDesc.type = "text";
  paramDesc.className = "bg-transparent outline-none";
  paramDesc.classList.add("param-row__desc");
  paramDescData.appendChild(paramDesc);
  newParamRow.appendChild(paramDescData);

  paramTableBody.appendChild(newParamRow);

  paramKey.addEventListener("input", (e) => {
    formatQuery();
    console.log(newParamRow.dataset.outdated);
    if (!newParamRow.dataset.outdated) {
      newParamRow.dataset.outdated = "YES";
      handleAddParamRow();
    }
  });
  addCheckboxListener();
};

paramKey.addEventListener("input", () => {
  formatQuery();
  if (!lastParamRow.dataset.outdated) {
    lastParamRow.dataset.outdated = "YES";
    handleAddParamRow();
  }
});

function formatQuery() {
  try {
    const paramRows = document.querySelectorAll(".param-row");
    const data = Array.from(paramRows).map((row) => ({
      checked: row.querySelector(".param-row__check").checked,
      key: row.querySelector(".param-row__key").value,
      value: row.querySelector(".param-row__desc").value,
    }));
    // console.log(data);
    const filteredData = data.filter(
      (row) => !!row.key && !!row.value && row.checked
    );
    const urlString = urlInput.value;
    const url = new URL(urlString);
    const domain = url.origin + url.pathname;

    console.log(url);

    urlInput.value =
      domain +
      "?" +
      filteredData.map((data) => `${data.key}=${data.value}`).join("&");
  } catch (error) {
    console.log(error);
  }
}
requestForm.addEventListener("submit", handleSendRequest);

const save = () => {
  const paramRows = document.querySelectorAll(".param-row");
  const data = Array.from(paramRows).map((row) => ({
    checked: row.querySelector(".param-row__check").checked,
    key: row.querySelector(".param-row__key").value,
    value: row.querySelector(".param-row__desc").value,
  }));

  localStorage.setItem(
    "saved",
    JSON.stringify([
      ...JSON.parse(localStorage.getItem("saved") || "[]"),
      {
        url: urlInput.value,
        method: requestMethodSelect.value,
        body: requestBody.value,
        params: data,
      },
    ])
  );
  generateSaved();
};

saveBtn.addEventListener("click", save);

const savedList = document.querySelector("#saved-list");

function generateSaved() {
  savedList.innerHTML = "";
  const savedQueries = JSON.parse(localStorage.getItem("saved") || "[]");
  console.log(savedQueries);
  savedQueries.forEach((qu) => {
    const listItem = document.createElement("li");
    listItem.textContent = qu.url || "Unknown";
    savedList.appendChild(listItem);
    listItem.dataset.value = JSON.stringify(qu);
    listItem.classList.add("saved-query");
    listItem.addEventListener("click", (e) => {
      const data = JSON.parse(e.target.dataset.value);
      urlInput.value = data.url;
      requestMethodSelect.value = data.method;
    });
  });
}

generateSaved();
